package com.yy;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

public class DllLoader {
    public interface Loader extends Library {
        public static Loader instance = (Loader) Native.load("libjuly-c.dll", Loader.class);

        public int initC(int num);
    }

    public static void main(String[] args) {
        Loader.instance.initC(99);
    }
}
