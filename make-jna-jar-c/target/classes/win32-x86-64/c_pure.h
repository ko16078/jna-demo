#ifndef C_PURE_H
#define C_PURE_H

#ifdef WIN32
#ifdef DLL_EXPORTS
      #define SIMPLE_CLASS_EXPORT __declspec(dllexport)
#else
      #define SIMPLE_CLASS_EXPORT __declspec(dllimport)
#endif
#else
      #define SIMPLE_CLASS_EXPORT
#endif

#include <string>
#include <vector>


using namespace std;

#include <stdio.h>

#ifdef __cplusplus
extern "C"{
SIMPLE_CLASS_EXPORT int initC(int num);
}
#endif


#endif
