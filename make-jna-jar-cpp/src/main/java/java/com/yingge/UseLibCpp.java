package com.yingge;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

import java.util.ArrayList;
import java.util.List;

public class UseLibCpp {
    public interface Loader extends Library {
        public static Loader instance = (Loader) Native.load("libjuly_cpp.dll", Loader.class);

        /*
        DLL_EXPORT CPPClass *getCppClass();

        DLL_EXPORT void adapterDestory(CPPClass* instance);

        DLL_EXPORT void adapterIncrease(CPPClass* instance);

        DLL_EXPORT int adapterGetNum(CPPClass* instance);
//数据传递测试
        DLL_EXPORT void adapterGetInt(CPPClass* instance,int* data);

        DLL_EXPORT void adapterGetString(CPPClass* instance,char* data);

        DLL_EXPORT void adapterAdd(CPPClass* instance,int a,int b,int* c,char** msg);
        */
        //函数名必须和C接口一致。
        Pointer getCppClass();

        void adapterDestory(Pointer dllInstance);

        void adapterIncrease(Pointer dllInstance);

        int adapterGetNum(Pointer dllInstance);

//        void adapterGetInt(Pointer dllInstance,char* data);
//        void adapterGetString(Pointer dllInstance,int* data);

        void adapterGetInt(Pointer dllInstance, IntByReference data);

        //        void adapterGetString(Pointer dllInstance, String str);
        void adapterGetString(Pointer dllInstance, PointerByReference data);

        //        DLL_EXPORT void adapterAdd(CPPClass* instance,int a,int b,int* c,char** msg);
        void adapterAdd(Pointer dllInstance, int a, int b, IntByReference c, PointerByReference msg);
    }

    //
    private Pointer instance;

    public UseLibCpp() {
        instance = Loader.instance.getCppClass();
    }

    public void destory() {
        Loader.instance.adapterDestory(instance);
    }

    public void increase() {
        Loader.instance.adapterIncrease(instance);
    }

    public int getNum() {
        return Loader.instance.adapterGetNum(instance);
    }

//    public void getInt(int[] data){
//        Loader.instance.adapterGetInt(instance,data);
//    }

    public void getInt(int data) {
        IntByReference tmpData = new IntByReference();
        Loader.instance.adapterGetInt(instance, tmpData);

        data = tmpData.getValue();
    }

    public void add(int a, int b, ArrayList<Integer> c, ArrayList<String> msg) {
        IntByReference ir = new IntByReference();
        PointerByReference pr = new PointerByReference();

        Loader.instance.adapterAdd(instance, a, b, ir, pr);

        System.out.println("c的值：" + ir.getValue());
        System.out.println("msg的值：" + pr.getValue().getString(0));

//        c = new Integer(ir.getValue());
//        msg = new String(pr.getValue().getString(0));

//        c = new ArrayList<Integer>();
        c.add(ir.getValue());

//        msg = new ArrayList<String>();
        msg.add(pr.getValue().getString(0));
    }

    public void getString(String str) {
//        PointerByReference pr = new PointerByReference();
//        Loader.instance.adapterGetString(instance, pr);
//        str = new String(pr.getValue().getString(0));

//        byte[] bytes = new byte[100];
//        Loader.instance.adapterGetString(instance, bytes);
//
//        str = new String(bytes);
    }

    public static void main(String[] args) {
        UseLibCpp dl = new UseLibCpp();
        dl.increase();
        System.out.println(dl.getNum());
        System.out.println("----------------------------------------------------------");
        int a = 1;
        int b = 2;
//        int c = new Integer(0);
//        String str = new String();
//        ArrayList<Integer> sum = null;
//        ArrayList<String> str = null;

        ArrayList<Integer> sum = new ArrayList<Integer>();
        ArrayList<String> str = new ArrayList<String>();
        dl.add(a, b, sum, str);
        System.out.println(sum.get(0));
        System.out.println(str.get(0));

        /*
        System.out.println("----------------------------------------------------------");
//        int[] num = new int[1];
        int num = new Integer(0);
        dl.getInt(num);
        System.out.println(num);

        System.out.println("----------------------------------------------------------");
        String str = new String();
        dl.getString(str);
        System.out.println(str);
         */
    }
}
