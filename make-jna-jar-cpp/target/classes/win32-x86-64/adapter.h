#ifndef ADAPTER_H
#define ADAPTER_H

#include "july_cpp.h"

#include <stdio.h>

#include <string>
#include <vector>

using namespace std;

#ifdef WIN32
#   define DLL_EXPORT __declspec(dllexport)
#else
#   define DLL_EXPORT
#endif

//
#ifdef __cplusplus
extern "C"{
DLL_EXPORT CPPClass *getCppClass();

DLL_EXPORT void adapterDestory(CPPClass* instance);

DLL_EXPORT void adapterIncrease(CPPClass* instance);

DLL_EXPORT int adapterGetNum(CPPClass* instance);
//数据传递测试
DLL_EXPORT void adapterGetInt(CPPClass* instance,int* data);

DLL_EXPORT void adapterGetString(CPPClass* instance,char* data);

DLL_EXPORT void adapterAdd(CPPClass* instance,int a,int b,int* c,char** msg);
}
#endif

#endif
