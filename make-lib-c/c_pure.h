#ifndef C_PURE_H
#define C_PURE_H

#ifdef WIN32
#   define DLL_EXPORT __declspec(dllexport)
#else
#   define DLL_EXPORT
#endif

#include <string>
#include <vector>


using namespace std;

#include <stdio.h>

#ifdef __cplusplus
extern "C"{
DLL_EXPORT int initC(int num);
#if 0
DLL_EXPORT void getChar(char* data);
DLL_EXPORT void getString(char* data);
DLL_EXPORT void getInt(int* data);
#endif
}
#endif


#endif
