#include "adapter.h"

CPPClass *getCppClass()
{
return new CPPClass();
}

void adapterDestory(CPPClass *instance)
{
    delete instance;
    instance = nullptr;
}

void adapterIncrease(CPPClass *instance)
{
instance->increase();
}

int adapterGetNum(CPPClass *instance)
{
 return instance->getNum();
}

void adapterGetString(CPPClass *instance, char *data)
{
 instance->getString(data);
}

void adapterGetInt(CPPClass *instance, int *data)
{
  instance->getInt(data);
}

void adapterAdd(CPPClass *instance, int a, int b, int *c, char **msg)
{
    instance->add(a,b,c,msg);
}
