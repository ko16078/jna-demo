#include "july_cpp.h"

#include <iostream>
#include <string>

using namespace std;

void CPPClass::increase()
{
    ++_num;
}

int CPPClass::getNum()
{
    return _num;
}

void CPPClass::getInt(int *data)
{
    if(data == NULL){
        cout<<("getInt,data is NULL.\n")<<endl;
        return;
    }

    (*data) = 99;
}

void CPPClass::getString(char *data)
{
    if(data == NULL){
        cout<<("getString,data is NULL.\n")<<endl;
        return;
    }

    string str("hello");

    memcpy(data,str.data(),str.size());
}

int CPPClass::add(int a, int b, int *c, char **msg) {
    *c = (a + b) * 2;
    char* string = "hello world!";
    *msg = string;
    return a + b;
}
