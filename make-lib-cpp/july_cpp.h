#ifndef JULY_CPP_H
#define JULY_CPP_H

#include <string>
#include <vector>
#include <iostream>

using namespace std;

class CPPClass{
public:
    CPPClass():_num(0){}
    CPPClass(int num):_num(num){}

    void increase();
    int getNum();

    void getInt(int* data);
    void getString(char* data);

    int add(int a, int b, int *c, char **msg);

#if 0
    void increase(){++_num;}
    int getNum(){return _num;}

    void getInt(int* data){
        if(data == NULL){
            cout<<("getInt,data is NULL.\n")<<endl;
            return;
        }

        (*data) = 99;
    }

    void getString(char* data){
        if(data == NULL){
            cout<<("getString,data is NULL.\n")<<endl;
            return;
        }

        string str("hello");

        memcpy(data,str.data(),str.size());
    }
#endif
private:
    int _num;
};

#endif





