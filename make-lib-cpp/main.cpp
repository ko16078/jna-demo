#include <iostream>
#include <string>

#include "adapter.h"
#include "july_cpp.h"

using namespace std;

int main(int argc,char **argv){
    cout<<"test..."<<endl;

#if 0
    CPPClass* cc = new CPPClass(99);
    //    CPPClass* cc = new CPPClass();
    cc->increase();
    cout<<cc->getNum()<<endl;
#endif

    CPPClass* cc = getCppClass();
//    cc->increase();
//    cout<<cc->getNum()<<endl;


#if 1
    int num = 0;
    adapterGetInt(cc,&num);
    cout<<num<<endl;

    char array[100] = {'0'};
    adapterGetString(cc,array);
    cout<<string(array)<<endl;
#endif

    int a = 1;
    int b = 2;
    int *c = new int;
    char * msg = new char(10);

    adapterAdd(cc,a,b,c,&msg);
    cout<<*c<<endl;
    cout<<string(msg)<<endl;

    return 0;
}

